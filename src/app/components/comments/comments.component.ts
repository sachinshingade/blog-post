import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  @Input() id: any;
  comments: any;
  constructor(
    private api: ApiService,
    private router: Router,
  ) { }

  ngOnInit() {
    console.log("IDCOO", this.id)

    this.api.getPostCommentts(this.id)
      .then((resp: any) => {
        console.log("RESP coo", resp);
        this.comments = resp.data;
        // this.postsData = resp.data;
        // tslint:disable-next-line: align
        // if (this.postsData) {
        //   // this.api.getUserInfo(this.postsData.user_id)
        //   //   .then((resp: any) => {
        //   //     console.log("user", resp);
        //   //     this.postsData['authorname'] = resp.data.name;
        //   //   });
        // }
      });

  }

}
