import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  postsData: any;
  postId: any;
  loading: Boolean;

  constructor(
    private api: ApiService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.postId = params['id'];
      this.getPostInfo(this.postId);
    })
  }


  private getPostInfo(postId) {
    this.loading = true;
    this.api.getPostDetails(postId)
      .then((resp: any) => {
        console.log("RESP", resp);
        this.postsData = resp.data;
        // tslint:disable-next-line: align
        if (this.postsData) {
          this.api.getUserInfo(this.postsData.user_id)
            .then((resp: any) => {
              this.loading = false;
              console.log("user", resp);
              this.postsData['authorname'] = resp.data.name;
            });
        }
      });
  }
}
