import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-postlist',
  templateUrl: './postlist.component.html',
  styleUrls: ['./postlist.component.scss']
})
export class PostlistComponent implements OnInit {
  postsData: any;
  page: any = 1;
  pagination: any;
  loading: Boolean;

  constructor(
    private api: ApiService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getPostList();
  }

  private getPostList(page?) {
    let pageno = 1;
    if (page) {
      pageno = page;
    }
    this.loading = true;
    this.api.getPostList(pageno)
      .then((resp: any) => {
        console.log("RESP", resp);
        this.postsData = resp.data;
        this.pagination = resp.meta.pagination;
        this.postsData.map((post) => {
          this.api.getUserInfo(post.user_id)
            .then((resp: any) => {
              console.log("user", resp);
              post['authorname'] = resp.data.name;
            });
          post['slicedBody'] = post.body.slice(0, 120) + "...";
          this.loading =  false;
          window.scrollTo(0, 0);
        });
      });
  }

  readMore(id) {
    this.router.navigate(['/Post',id])
  }

  onPageChange(type) {
    if (type == 'prev') {
      if (this.page == 1) {
        return
      } else {
        this.page =  this.page - 1;
        this.getPostList(this.page)
      }
    } else {
      if (this.page <= this.pagination.pages) {
        ++this.page;
        this.getPostList(this.page)
      } else {
        return
      }
    }
  }

}
