import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient,
  ) { }

  getPostList(pageno) {
    // let o = obj;
    var options:any = {'params': {'page': pageno}};
    let headers = new HttpHeaders({'Content-Type': 'Application/Json'});
    headers.append('Access-Control-Allow-Origin', '*');
    options.headers = headers;
    return new Promise((resolve, reject) => {
      this.http.get(environment.api_point + 'posts', options).subscribe(async (r: any) => {
        console.log("post data ", r);
        if(r.code === 200){
          resolve(r);
        } else {
          reject(r);
          alert(r.data.message);
        }
      });
    })
  }

  getUserInfo(id) {
    // users/123
    var options:any = {};
    let headers = new HttpHeaders({'Content-Type': 'Application/Json'});
    headers.append('Access-Control-Allow-Origin', '*');
    options.headers = headers;
    return new Promise((resolve, reject) => {
      this.http.get(environment.api_point + 'users/'+ id, options).subscribe(async (r: any) => {
        console.log("post data ", r);
        if(r.code === 200){
          resolve(r);
        } else {
          reject(r);
          alert(r.data.message);
        }
      });
    })
  }
  getPostDetails(id){
    var options:any = {};
    let headers = new HttpHeaders({'Content-Type': 'Application/Json'});
    headers.append('Access-Control-Allow-Origin', '*');
    options.headers = headers;
    return new Promise((resolve, reject) => {
      this.http.get(environment.api_point + 'posts/'+ id, options).subscribe(async (r: any) => {
        console.log("post data ", r);
        if(r.code === 200){
          resolve(r);
        } else {
          reject(r);
          alert(r.data.message);
        }
      });
    })
  }
  getPostCommentts(id) {
    var options:any = {};
    let headers = new HttpHeaders({'Content-Type': 'Application/Json'});
    headers.append('Access-Control-Allow-Origin', '*');
    options.headers = headers;
    return new Promise((resolve, reject) => {
      this.http.get(environment.api_point + `posts/${id}/comments`, options).subscribe(async (r: any) => {
        console.log("post data cooo", r);
        if(r.code === 200){
          resolve(r);
        } else {
          reject(r);
          alert(r.data.message);
        }
      });
    })
  }
}
