import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostlistComponent } from './views/postlist/postlist.component';
import { PostComponent } from './views/post/post.component';


const routes: Routes = [
  {
    path: "",
    "redirectTo" : "/Home",
    "pathMatch" :'full'
  },
  {
    "path" : "Home",
    "component" : PostlistComponent,
  },
  {
    "path" : "Post/:id",
    "component" : PostComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
